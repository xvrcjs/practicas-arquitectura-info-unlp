;Memoria de datos
org 1000h
  num1_l dw 01h
  num1_h dw 00h
  num2_l dw 03h
  num2_h dw 00h
  res_l dw ?
  res_h dw ?

;Subrutina sum32
org 3000h
  sum32: push bx
  	 mov bx, sp
	 push ax
	 push cx
	 push dx
	 add bx, 6
	 mov cx, [bx]
	 add bx, 4
	 mov ax, [bx]
	 add ax, cx
	 push ax
	 pushf
	 sub bx, 2
	 mov cx, [bx]
	 add bx, 4
	 mov dx, [bx]
	 popf
	 adc dx,cx
	 pop ax
	 add bx, 2
	 mov [bx], ax
	 add bx, 2
	 mov [bx], dx
	 pop dx
	 pop cx
	 pop ax
  ret

;Programa principal
org 2000h
  mov ax, res_h
  push ax
  mov ax, res_l
  push ax
  mov ax, num1_h
  push ax
  mov ax, num1_l
  push ax
  mov ax, num2_h
  push ax
  mov ax, num2_l
  push ax
  call sum32
  pop ax
  pop ax
  pop ax
  pop ax
  pop ax
  mov res_l, ax
  pop ax
  mov res_h, ax
  hlt
  end
