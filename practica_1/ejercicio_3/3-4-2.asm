;Memoria de datos
org 1000h
  num1_l dw 01h
  num1_h dw 00h
  num2_l dw 03h
  num2_h dw 00h
  res_l dw ?
  res_h dw ?

;Subrutina sum32
org 3000h
  sum32: push bx
  	 mov bx, sp
	 push ax
	 push cx
	 push dx
	 add bx, 4
	 mov ax, [bx]
	 push bx
	 mov bx, ax
	 mov dx, [bx]
	 pop bx
	 add bx, 4
	 mov ax, [bx]
	 push bx
	 mov bx, ax
	 mov cx, [bx]
	 add cx, dx
	 pop bx
	 pushf
	 add bx, 4
	 mov ax, [bx]
	 push bx
	 mov bx, ax
	 mov [bx], cx
	 pop bx
	 sub bx, 6
	 mov ax, [bx]
	 push bx
	 mov bx, ax
	 mov dx, [bx]
	 pop bx
	 add bx, 4
	 mov ax, [bx]
	 push bx
	 mov bx, ax
	 mov cx, [bx]
	 pop bx
	 popf
	 adc cx, dx
	 add bx, 4
	 mov ax, [bx]
	 mov bx, ax
	 mov [bx], cx
	 pop dx
	 pop cx
	 pop ax
	 pop bx
  ret

;Programa principal
org 2000h
  mov ax, offset res_h
  push ax
  mov ax, offset res_l
  push ax
  mov ax, offset num1_h
  push ax
  mov ax, offset num1_l
  push ax
  mov ax, offset num2_h
  push ax
  mov ax, offset num2_l
  push ax
  call sum32
  pop ax
  pop ax
  pop ax
  pop ax
  pop ax
  pop ax
  mov ax, res_h
  mov bx, res_l
  hlt
  end
