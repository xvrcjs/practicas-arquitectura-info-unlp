;Memoria de datos
org 1000h
  num1_l dw 01h
  num1_h dw 00h
  num2_l dw 03h
  num2_h dw 00h
  res_l dw ?
  res_h dw ?

;Subrutina sum32
org 3000h
  sum32: add ax, bx
  	 adc cx, dx
  ret

;Programa principal
org 2000h
  mov ax, num1_l
  mov bx, num2_l
  mov cx, num1_h
  mov dx, num2_h
  call sum32
  mov res_l, ax
  mov res_h, cx
  hlt
  end
