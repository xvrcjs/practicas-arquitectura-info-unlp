;Memoria de datos
org 1000h
  num1_l dw 01h
  num1_h dw 00h
  num2_l dw 03h
  num2_h dw 00h
  res_l dw ?
  res_h dw ?

;Subrutina sum32
org 3000h
  sum32: mov ax, [bx]
  	 add bx, 2
	 push bx
	 mov bx, cx
	 mov cx, [bx]
	 add bx, 2
	 push bx
	 add ax, cx
	 mov bx, dx
	 mov [bx], ax
	 pop bx
	 mov ax, [bx]
	 pop bx
	 mov cx, [bx]
	 adc ax, cx
	 mov bx, dx
	 add bx, 2
	 mov [bx], ax
  ret


;Programa principal
org 2000h
  mov bx, offset num1_l
  mov cx, offset num2_l
  mov dx, offset res_l
  call sum32
  hlt
  end
