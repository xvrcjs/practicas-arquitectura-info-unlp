;Memoria de datos
org 1000h
  num1_l dw 01h
  num1_h dw 00h
  num2_l dw 03h
  num2_h dw 00h
  res_l dw ?
  res_h dw ?

;Programa principal
org 2000h
  mov ax, num1_l
  mov cx, num2_l
  add ax, cx
  mov dx, num1_h
  mov cx, num2_h
  adc dx, cx
  mov res_l, ax
  mov res_h, dx
  hlt
  end
