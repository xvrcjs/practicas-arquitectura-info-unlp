; Memoria de Datos
org 1000h
  num1 db 5h
  num2 db 3h
;Memoria de Instrucciones
org 2000h
  mov al, num1
  cmp al, 0
  jz fin
  mov ah, 0
  mov dx, 0
  mov cl, num2
  loop: cmp cl, 0
        jz fin
        add dx, ax
        dec cl
        jmp loop
  fin: hlt
  end
