;Memoria de datos
org 1000h
  num1 dw 5h ;num1 y num2 deben ser mayores a cero
  num2 dw 3h

;Memoria de instrucciones
org 3000h
  mul:  mov dx, 0
    lazo: mov bx, ax
          add dx, [bx]
          push dx
         mov bx, cx
         mov dx, [bx]
         dec dx
         mov [bx], dx
         pop dx
    jnz lazo
  ret

;Programa principal
org 2000h
  mov ax, offset num1
  mov cx, offset num2
  call mul
  hlt
  end
