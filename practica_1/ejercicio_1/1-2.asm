;Memoria de Datos
org 1000h
  num1 db 5h
  num2 db 3h

;Memoria de Instrucciones
org 3000h ;subrutina MUL
  mul:  cmp al, 0
        jz fin
        cmp cl, 0
        jz fin
        mov ah, 0
        mov dx, 0
  lazo: add dx, ax
        dec cl
        jnz lazo
  fin:  ret

org 2000h ;Programa principal
  mov al, num1
  mov cl, num2
  call mul
  hlt
  end
