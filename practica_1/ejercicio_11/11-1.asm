org 1000h
  cadena db "AaEeIiOoUu"
  car db "E"

org 3000h
  es_vocal: mov dl, 000H
    lazo: cmp al, [bx]
         jz cumple
	 inc bx
	 dec cl
	 jnz lazo
	 jmp fin
	 cumple: mov dl, 0ffh
     fin: ret

org 2000h
  mov bx, offset cadena
  mov al, car
  mov cl, offset car - offset cadena
  call es_vocal
  hlt
  end
