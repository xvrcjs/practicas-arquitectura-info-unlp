;Memoria de datos
org 1000h
    num1 dw 5h
    num2 dw 3h
    res dw ?

;subrutina mul
org 3000h
    mul: push bx
         mov bx, sp
	 push cx
	 push ax
	 push dx
	 add bx, 6
	 mov cx, [bx]
	 add bx, 2
	 mov ax, [bx]
	 suma: add dx, ax
	       dec cx
	       jnz suma
	 sub bx, 4
	 mov ax, [bx]
	 mov bx, ax
	 mov [bx], dx
	 pop dx
	 pop ax
	 pop cx
	 pop bx
    ret

;Programa principal
org 2000h
    mov ax, num1
    push ax
    mov ax, num2
    push ax
    mov ax, offset res
    push ax
    mov dx, 0
    call mul
    pop ax
    pop ax
    pop ax
    hlt
    end
