;Memoria de datos
org 1000h
  vocales db "AaEeIiOoUu"
  cadena db "Arquitectura de Computadoras"
  fin_cadena db ?

;SubRutina es_vocal
org 3000h
  es_vocal: mov bx, offset vocales
    mov dl, 000H
    lazo: cmp al, [bx]
         jz cumple
	 inc bx
	 dec cl
	 jnz lazo
	 jmp fin_es_vocal
	 cumple: mov dl, 0ffh
     fin_es_vocal: ret

;subRutina contar-vocales
org 4000h
  contar_vocales: mov ah, 000h
    ciclo: mov al, [bx]
      inc bx
      push bx
      call es_vocal
      cmp dl, 0ffh
      jnz no_contar
      inc ah
      no_contar: pop bx
      dec ch
      jnz ciclo
  ret

;Programa principal
org 2000h
  mov cl, offset cadena - offset vocales ;cantidad de vocales
  mov ch, offset fin_cadena - offset cadena ;cantidad de caracteres de la cadena
  mov bx, offset cadena
  call contar_vocales
  hlt
  end
